package spawn

import (
	"testing"
	"github.com/stretchr/testify/assert"
	
	"github.com/mholt/caddy"
)

func TestParseConfigs(t *testing.T) {
	type TestCase struct {
		input    string
		expected []*Spawner
	}
	
	testcases := []TestCase {
		{ "spawn /", []*Spawner { &Spawner {
			Path: "/",
			Pwd: ".",
		} } },
		{ `spawn /bar {
				run python app.py <port>
				without /bar
				except /foo /oof
			}`, []*Spawner {
				&Spawner {
					Path: "/bar",
					RunName: "python",
					RunArgs: []string{ "app.py", "<port>" },
					WithoutPrefix: "/bar",
					Except: []string{ "/foo", "/oof" },
					Pwd: ".",
		} } },
		{ `spawn /first {
				except /not1
			}
			spawn /second/path {
				run foo <port> bar
				max-processes 3
				max-conns-per-process 10
			}`, []*Spawner {
				&Spawner {
					Path: "/first",
					Except: []string{ "/not1" },
					Pwd: ".",
				},
				&Spawner {
					Path: "/second/path",
					RunName: "foo",
					RunArgs: []string{ "<port>", "bar" },
					Pwd: ".",
					MaxProcesses: 3,
					MaxConnsPerProcess: 10,
		} } },
	}
	
	for _, test := range testcases {
		controller := caddy.NewTestController("http", test.input)
		actual, err := ParseConfig(controller)
		if err != nil {
			t.Errorf("ParseConfig return err: %v", err)
		}
		for _, spawner := range test.expected {
			if spawner.Except == nil {
				spawner.Except = []string{}
			}
			if spawner.Processes == nil {
				spawner.Processes = []*Process{}
			}
			if spawner.RunArgs == nil {
				spawner.RunArgs = []string{}
			}
		}
		assert.Equal(t, test.expected, actual)
	}
}
