package spawn

import (
	"strconv"
	
	"github.com/mholt/caddy"
	"github.com/mholt/caddy/caddyhttp/httpserver"
)

func ParseConfig(c *caddy.Controller) ([]*Spawner, error) {
	cfg := httpserver.GetConfig(c)
	root := cfg.Root
	
	var spawners []*Spawner
	
	d := c.Dispenser
	for d.Next() {
		spawner := &Spawner {
			Except: []string{},
			Pwd: root,
			Processes: []*Process{},
			RunArgs: []string{},
		}
		
		if !d.Args(&spawner.Path) {
			return spawners, d.ArgErr()
		}
		
		if len(d.RemainingArgs()) > 0 {
			return spawners, d.ArgErr()
		}
		
		for d.NextBlock() {
			key := d.Val()
			args := d.RemainingArgs()
			
			switch key {
				case "except":
					spawner.Except = append(spawner.Except, args...)
				case "run":
					spawner.RunName = args[0]
					spawner.RunArgs = args[1:]
				case "without":
					if len(args) != 1 {
						return spawners, d.ArgErr()
					}
					spawner.WithoutPrefix = args[0]
				case "max-processes":
					if len(args) != 1 {
						return spawners, d.ArgErr()
					}
					i, err := strconv.Atoi(args[0])
					if err == nil {
						spawner.MaxProcesses = int32(i)
					}
				case "max-conns-per-process":
					if len(args) != 1 {
						return spawners, d.ArgErr()
					}
					i, err := strconv.Atoi(args[0])
					if err == nil {
						spawner.MaxConnsPerProcess = int32(i)
					}
				default:
					return spawners, d.ArgErr()
			}
		}
		
		spawners = append(spawners, spawner)
	}
	
	return spawners, nil
}
