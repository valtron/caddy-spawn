#!/bin/sh

set -e

trap on_exit EXIT

function on_exit() {
	kill -2 $PID_CADDY
}

#go install github.com/mholt/caddy/caddy
$GOPATH/bin/caddy -log stderr -conf Caddyfile &
PID_CADDY=$!

curl -s 127.0.0.1:8000/foo/bar?a=1 > out1.txt &
curl -s 127.0.0.1:8000/foo/bar?a=2 > out2.txt &
curl -s 127.0.0.1:8000/foo/bar?a=3 > out3.txt &
curl -s 127.0.0.1:8000/foo/bar?a=4 > out4.txt &
curl -s 127.0.0.1:8000/foo/bar?a=5 > out5.txt &
curl -s 127.0.0.1:8000/foo/bar?a=6 > out6.txt &
curl -s 127.0.0.1:8000/foo/bar?a=7 > out7.txt &
curl -s 127.0.0.1:8000/foo/bar?a=8 > out8.txt &
#curl -s 127.0.0.1:8000/robots.txt

sleep 3
