package spawn

import (
	"github.com/mholt/caddy"
	"github.com/mholt/caddy/caddyhttp/httpserver"
)

func init() {
	caddy.RegisterPlugin("spawn", caddy.Plugin {
		ServerType: "http", Action: setup,
	})
}

func setup(c *caddy.Controller) error {
	spawners, err := ParseConfig(c)
	
	if err != nil {
		return err
	}
	
	c.OnShutdown(func () error {
		for _, spawner := range spawners {
			spawner.Close()
		}
		return nil
	})
	
	mid := func (next httpserver.Handler) httpserver.Handler {
		return Handler {
			Next: next, Spawners: spawners,
		}
	}
	
	httpserver.GetConfig(c).AddMiddleware(mid)
	
	return nil
}
